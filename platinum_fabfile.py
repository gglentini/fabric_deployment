"""
Fabric deployment routines for Platinum.

The module contains commandline commands to easily deliver any Platinum components on any host.

"""

from fabric.api import *
from fabric.contrib.console import confirm

from StringIO import StringIO


DASHBOARD_CODE_DIR = "dashboard/django/opm"

CLI_DASH_DELIVERY_PATH = "/path/to/commandline/"
DASHBOARD_DELIVERY_PATH = "/path/to/dashboard/"

LATEST_DELIVERY_PATH = "/path/to/latest/repository/"
OA_DELIVERY_PATH = "/path/to/oa/"

#INSTANCE_NAME = "regllen"
#dash_unittest_cmd = "./manage.py jenkins platinum accounts --instance=%s --skipdb" % (INSTANCE_NAME)

DELIVERY_TYPE = {'cli': 'platinum_commandline',
                 'dashboard':'platinum_dashboard'
                 }

DELIVERY_PATHS = {'extra': [LATEST_DELIVERY_PATH, OA_DELIVERY_PATH],
                  'cli': [CLI_DASH_DELIVERY_PATH],
                  'dashboard':[DASHBOARD_DELIVERY_PATH],
                  }

DELIVERY_USERS = {'common':'common_user',
                  'oa':'oa_user',
                  }

PLATINUM_HOSTS = {'dev': ['dev01'],
                  'test': ['test01'],
                  'prod': ['prod01']
                  }

#class PlatinumDeliveryConfig(host,type):
#    def __init__(self, host, type):
#        self.hosts = PLATINUM_HOSTS.get(host)
#        env.hosts = self.hosts
#        self.paths = DELIVERY_PATHS.get(type)
    
#class PlatinumDeliveryError(Exception):
##    def __init__(self, value):
##        self.value = value
##    def __str__(self):
##        return repr(self.value)
#    pass


def _check_allowed_hosts_and_ask():
    not_allowed_hosts = PLATINUM_HOSTS['dev'][:]
    not_allowed_hosts.extend(PLATINUM_HOSTS['test'])
    if (set(env.hosts).issubset(not_allowed_hosts)):
        if not confirm("You are trying to deliver to %s, "
            "delivery of Platinum CLI in latest and OA repository should be avoided in this case. Continue anyway?" % env.hosts):
            abort("Aborting at user request.")

def _check_allowed_hosts_and_abort():
    not_allowed_hosts = PLATINUM_HOSTS['dev'][:]
    not_allowed_hosts.extend(PLATINUM_HOSTS['test'])
    if (set(env.hosts).issubset(not_allowed_hosts)):
        abort("You are trying to deliver to %s, "
            "delivery of Platinum CLI in latest and OA repository should be avoided in this case."
            "\nTry fab prod COMMAND or fab COMMAND if you really want to deliver to latest/OA." % env.hosts)

def prod():
    """Settings for Platinum production host."""
    env.user = DELIVERY_USERS['common']
    env.hosts = PLATINUM_HOSTS['prod']

def dev():
    """Settings for Platinum development host."""
    env.user = DELIVERY_USERS['common']
    env.hosts = PLATINUM_HOSTS['dev']
    
def test():
    """Settings for Platinum test host."""
    env.user = DELIVERY_USERS['common']
    env.hosts = PLATINUM_HOSTS['test']
        
def stop_apache():
    """Stop Apache server."""
    sudo("/etc/init.d/apache_pt1 stop")
    
def stop_nginx():
    """Stop nginx server."""
    sudo("/etc/init.d/nginx stop")
    
def start_apache():
    """Start Apache server."""
    sudo("/etc/init.d/apache_pt1 start")

def start_nginx():
    """Start nginx server."""
    sudo("/etc/init.d/nginx start")
    
def restart():
    '''Stop and restart Apache server.'''
    stop_apache()
    start_apache()
    
def clean_pyc_files():
    """Remove existing .pyc files."""
    # django_extensions_0.9 contains a manage.py command clean_pyc(), it could be interesting to replace this...
    run('find . -name "*.pyc" -exec rm {} \;')
  
def clean_kerberos_cache():
    """"Clean cached kerberos tickets on a Platinum host.
        
        Usage:
            fab [dev|test|prod] clean_kerberos_cache
    """
    tickets_strs = StringIO()
    tickets_strs = run('find /tmp -name "krb5cc_*3535*"')
    if tickets_strs:
        for ticket in tickets_strs.split('\r\n'):
            run('rm -rf %s' % ticket)
    
#def prepare_test():
#    local('hg clonept -P platinum_dashboard -b 1.0.x --instance=dsp')
#
#def run_test():
##    require('hosts', provided_by=[production])
#    print env.hosts
#    with lcd(DASHBOARD_CODE_DIR):
#        print os.getcwd()
#    with settings(warn_only=True):
#        with lcd(CODE_DIR):
#            result = local(dash_unittest_cmd)
#    if result.failed and not confirm("Tests failed. Continue anyway?"):
#        abort("Aborting at user request.")

def push():
    """Perform hg push."""
    run('/opt/devsup/mercurial/bin/hg push')

def pull():
    """Perform hg pull."""
    run('/opt/devsup/mercurial/bin/hg pull')
    
def update():
    """Perform hg update."""
    run('/opt/devsup/mercurial/bin/hg update')

def pull_and_update():
    """Perform hg pull -u."""
    pull()
    update()
    
def clone_or_revert(source, repo_path):
    """Perform hg clone_or_revert <src> <dst>."""
    run('/opt/devsup/mercurial/bin/hg clone_or_revert %s %s' %(source, repo_path))
    
def deploy_cli():
    """Deploy Platinum CLI in the repository server by Apache on a Platinum host.
        - platinum host CLI                       
        
        Basically a pull from the Platinum CLI integration repositories.
        
        Restarts also Apache after the code updates.
        
        To be used safely in any Platinum environment (dev, tst, prd), like
        
            fab [dev|test|prod] deploy_cli
    """
    delivery_paths = DELIVERY_PATHS['cli']
    with settings(warn_only=True):
        for delivery_path in delivery_paths:
            if run("test -d %s" % delivery_path).failed:
#            TODO: adding an hg clone in case the dest folder does not exist?
                abort("Delivery folder %s doesn't exist." % delivery_path)
            with cd(delivery_path):
                print "\nPull and update in %s " % delivery_path
                pull_and_update()
            clean_kerberos_cache()
            restart()

def deploy_dashboard():
    """Deploy Platinum Dashboard in the repository server by Apache and the platinum host CLI.
        - repository server by Apache             
        
        Basically a pull from the Platinum Dashboard integration repositories.
        
        Restarts also Apache server after the code updates.
        
        To be used safely in any Platinum environment (dev, tst, prd), like
        
            fab [dev|test|prod] deploy_dashboard
    """
    delivery_paths = DELIVERY_PATHS['dashboard']
    with settings(warn_only=True):
        for delivery_path in delivery_paths:
            if run("test -d %s" % delivery_path).failed:
#            TODO: adding an hg clone in case the dest folder does not exist?
                abort("Delivery folder %s doesn't exist." % delivery_path)
            with cd(delivery_path):
                print "\nPull and update in %s " % delivery_path
                pull_and_update()
                clean_pyc_files()
        clean_kerberos_cache()
        restart()
        
def deploy_dash_and_cli():
    """Deploy Platinum Dashboard in the repository server by Apache and the platinum host CLI.
        - repository server by Apache             
        - platinum host CLI                       
        
        Basically a pull from the Platinum Dashboard and CLI integration repositories.
        
        Restarts also Apache and nginx servers after the code updates.
        
        To be used safely in any Platinum environment (dev, tst, prd), like
        
            fab [dev|test|prod] deploy_dashboard_and_cli
    """
    delivery_paths = DELIVERY_PATHS['dashboard']+DELIVERY_PATHS['cli']
    with settings(warn_only=True):
        for delivery_path in delivery_paths:
            if run("test -d %s" % delivery_path).failed:
#            TODO: adding an hg clone in case the dest folder does not exist?
                abort("Delivery folder %s doesn't exist." % delivery_path)
            with cd(delivery_path):
                print "\nPull and update in %s :" % delivery_path
                pull_and_update()
                clean_pyc_files()
        clean_kerberos_cache()
        restart()
 
def deploy_cli_latest_and_oa():
    """Deploy Platinum CLI in latest repository and the location used by OA.
        - latest repository                          
        - the location used by OA for replication    
        
        Basically a pull from the Platinum CLI integration repository.
        
        It can be run from any machine.
        To be used ONLY to replicate Platinum CLI in every box AFTER (or in the same time) the delivery in prd environment.
            
            fab [prod] deploy_cli_latest_and_oa
    """
    _check_allowed_hosts_and_abort()
    delivery_paths = DELIVERY_PATHS['extra']
    with settings(warn_only=True):
        for delivery_path in delivery_paths:
            if run("test -d %s" % delivery_path).failed:
#                TODO: adding an hg clone in case the dest folder does not exist?
#                run("git clone user@vcshost:/path/to/repo/.git %s" % code_dir)
                abort("Delivery folder %s doesn't exist." % delivery_path)
            with cd(delivery_path):
#                TODO: maybe there's a better way to change user for OA delivery...anyway, actually it works
                print "\nPull and update in %s :" % delivery_path
                if delivery_path is OA_DELIVERY_PATH:
                    env.user = 'dsptools'
                pull_and_update()    

def deploy_all():
    """Deploy Platinum Dashboard and CLI in the specified host, then deploy Platinum CLI in latest and OA locations.
    
        To be used only for prod environment.
        
            fab [prod] deploy_all
    """
    _check_allowed_hosts_and_abort()
    deploy_dash_and_cli()
    deploy_cli_latest_and_oa()
    
def show_id():
    run('id')

    