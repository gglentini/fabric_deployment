"""
    Library use of fab scripts for Platinum deliveries.
    To be used as hooks in Platinum.
"""

#execute(X, hosts=Y)

from fabric.tasks import execute
from fabric.network import disconnect_all
import platinum_fabfile


PLATINUM_HOSTS_METHODS_MAP = {'dev' : platinum_fabfile.dev,
                              'test': platinum_fabfile.test,
                              'prod': platinum_fabfile.prod
                              }

def deploy_cli(hosts='dev'):
    try:
        execute(PLATINUM_HOSTS_METHODS_MAP[hosts])
        execute(platinum_fabfile.deploy_cli, hosts=platinum_fabfile.PLATINUM_HOSTS[hosts])
    finally:
        disconnect_all()

def deploy_dashboard(hosts='dev'):
    try:
        execute(PLATINUM_HOSTS_METHODS_MAP[hosts])
        execute(platinum_fabfile.deploy_dashboard, hosts=platinum_fabfile.PLATINUM_HOSTS[hosts])
    finally:
        disconnect_all()

def deploy_dash_and_cli(hosts='dev'):
    try:
        execute(PLATINUM_HOSTS_METHODS_MAP[hosts])
        execute(platinum_fabfile.deploy_dash_and_cli, hosts=platinum_fabfile.PLATINUM_HOSTS[hosts])
    finally:
        disconnect_all()
        
def deploy_cli_latest_and_oa(hosts='prod'):
    try:
        execute(PLATINUM_HOSTS_METHODS_MAP[hosts])
        execute(platinum_fabfile.deploy_cli_latest_and_oa, hosts=platinum_fabfile.PLATINUM_HOSTS[hosts])
    finally:
        disconnect_all()
        
def deploy_all(hosts='prod'):
    try:
        execute(PLATINUM_HOSTS_METHODS_MAP[hosts])
        execute(platinum_fabfile.deploy_all, hosts=platinum_fabfile.PLATINUM_HOSTS[hosts])
    finally:
        disconnect_all()

def clean_pyc_files(hosts='dev'):
    try:
        execute(PLATINUM_HOSTS_METHODS_MAP[hosts])
        execute(platinum_fabfile.clean_pyc_files, hosts=platinum_fabfile.PLATINUM_HOSTS[hosts])
    finally:
        disconnect_all()
        
def clean_kerberos_cache(hosts='dev'):
    try:
        execute(PLATINUM_HOSTS_METHODS_MAP[hosts])
        execute(platinum_fabfile.clean_kerberos_cache, hosts=platinum_fabfile.PLATINUM_HOSTS[hosts])
    finally:
        disconnect_all()   
        
def restart(hosts='dev'):
    try:
        execute(PLATINUM_HOSTS_METHODS_MAP[hosts])
        execute(platinum_fabfile.restart, hosts=platinum_fabfile.PLATINUM_HOSTS[hosts])
    finally:
        disconnect_all()    
        
def clone_or_revert(source, repo_path, hosts='dev'):
    """Execute hg clone_or_revert on Platinum dev|test|prod machine.
        Default host is Platinum dev.
    
        Example:
           clone_or_revert(hosts='test')
    """
    try:
        execute(PLATINUM_HOSTS_METHODS_MAP[hosts])
        execute(platinum_fabfile.clone_or_revert, source=source,repo_path=repo_path, hosts=platinum_fabfile.PLATINUM_HOSTS[hosts])
    finally:
        disconnect_all()
        
def platinum_pullrequest_rejection(branch, select_only_continuous_build_done_ok=True, dest=None):
    """Post action (hook) to be executed after the rejection of any Platinum pull request (dashboard or cli). 
        The steps accomplished are:
             1/ rebuild integration repository mimicking standard 'rebuild_int_repo' hook, taking only CB tested OK pending PRs
             2/ rebuild repo in Platinum dev host
             3/ restart also Platinum dev.
    """
    # rebuild integration repo mimicking standard 'rebuild_int_repo' hook
    print "Reverting integration repository: %s " % (branch.get_int_repository())
    from platinum_integration_api import rebuild_intergration_repository
    rebuild_intergration_repository(branch, select_only_continuous_build_done_ok, dest)
    
    # rebuild repo in Platinum dev host, taking integration repos as base
    # TODO: this could be done better...using something like DELIVERY_TYPE
        
    paths_tobe_reverted = platinum_fabfile.DELIVERY_PATHS['dashboard'] if 'dashboard' in branch.name else platinum_fabfile.DELIVERY_PATHS['cli']
    for path in paths_tobe_reverted:
        clone_or_revert(source=branch.get_int_repository(),repo_path=path)
    # TODO: ssh connection is reopened here...to be reviewed for optmization
    restart()
    
def platinum_pullrequest_validation(pullrequest, branch,verify_tested_done_ok=False):
    """Post action (hook) to be executed after the validation of any Platinum pull request (dashboard or cli). 
        The steps accomplished are:
             1/ push to reference repository mimicking standard 'tryfetch_pullrequest_on_main_branch' hook
             2/ push to Platinum test host
             3/ restart also Platinum test host.
    """
    # point 1/
    print "Fetching PR #%s in : %s" % (pullrequest.id, branch.get_main_repository())
    from platinum_integration_api import tryfetch_pullrequest_on_main_branch
    tryfetch_pullrequest_on_main_branch(pullrequest, branch, verify_tested_done_ok)
    
    # point 2/
    try:
        execute(PLATINUM_HOSTS_METHODS_MAP['test'])
        if 'dashboard' in branch.name:
            deploy_dashboard(hosts='test')
        else:
            deploy_cli(hosts='test')
    finally:
        disconnect_all()
       
    
    