#!/usr/bin/env python
"""
Post action (hook) to be executed after the rejection of any Platinum pull request (dashboard or cli). 
The steps accomplished are:
    1/ rebuild integration repo mimicking standard 'rebuild_int_repo' hook
    2/ rebuild repo in Platinum dev host
    3/ restart also Platinum dev.
"""

from platinum_api import HookMethodRunner

from platinum_fablib import platinum_pullrequest_rejection

import sys

sys.exit(0 if not HookMethodRunner().run(platinum_pullrequest_rejection) else -1)
