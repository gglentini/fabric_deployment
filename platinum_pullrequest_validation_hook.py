#!/usr/bin/env python
"""
Post action (hook) to be executed after the validation of any Platinum pull request (dashboard or cli). 
The steps accomplished are:
    1/ push to referencial repo mimicking standard 'tryfetch_pullrequest_on_main_branch' hook
    2/ push to Platinum test host
    3/ restart also Platinum test host.
"""

from platinum_api import HookMethodRunner

from platinum_fablib import platinum_pullrequest_validation

import sys

sys.exit(0 if not HookMethodRunner().run(platinum_pullrequest_validation) else -1)
